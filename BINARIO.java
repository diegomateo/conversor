import java.io.*;
 import java.text.*;
 public class BINARIO{
		public static void main(String []args)throws IOException{
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			{
				System.out.println("================================================");
				System.out.println("Diego Alexander Mateo Sinto     2013-312");
				System.out.println("================================================");
				System.out.println("BINARIO -----> DECIMAL");
				System.out.println("");
				System.out.print("INGRESA UN NUMERO BINARIO : ");
				String Bidi = in.readLine();
				int i = Integer.parseInt(Bidi,2);
				String dec = Integer.toString(i);
				System.out.println("");
				System.out.println("El valor en DECIMAL es: " + dec);
				System.out.println("================================================");
			}
		}
   }